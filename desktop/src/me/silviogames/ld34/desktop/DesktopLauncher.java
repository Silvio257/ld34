package me.silviogames.ld34.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import me.silviogames.ld34.Main;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.x = 200;
        config.width = Main.world_width * 3;
        config.height = Main.world_height * 3;
		new LwjglApplication(new Main(), config);
	}
}

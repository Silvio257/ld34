package me.silviogames.ld34;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Silvio on 13.12.2015.
 */
public class TickS extends State {

    float time = 0.4f, frametime = 0;
    int showpoints;
    boolean addpoints = true, up = true;

    int frame = 0;


    public TickS(Main main) {
        super(main);
        showpoints = MathUtils.floor(main.energy);
        main.energy += main.growth;
        main.step++;
        if(main.step > 300){
            main.step = 300;
        }

        PlantTile now = main.start_plant;
        while (now != null) {
            if (now.left != null) {
                now.left.tick(main);
            }
            if (now.right != null) {
                now.right.tick(main);
            }
            now = now.after;
        }

        //leafs might have died
        main.calculate_growth();


    }

    @Override
    protected void update(float delta) {
        if (addpoints) {
            time -= delta;
            if (time <= 0) {
                Res.Sounds.POINTS.play();
                time = 0.07f;
                showpoints++;
                if (showpoints >= MathUtils.floor(main.energy)) {
                    //next step
                    addpoints = false;
                }
            }
        }
        if (main.dead_leafs.size == 0 && !addpoints) {
            //fireattack?
            if (main.warning) {
                main.state = new FireAttack(main);
                main.warning = false;
            } else {
                if (MathUtils.random(350) < main.step && main.plant_length() > 12) {
                    main.warning = true;
                }
                main.state = new GameS(main);
            }
        }
        for (int index = main.dead_leafs.size - 1; index >= 0; index--) {
            Vector2 p = main.dead_leafs.get(index);
            p.y -= delta * 30;
            if (p.y < -10) {
                main.dead_leafs.removeIndex(index);
            }
        }

        frametime += delta;
        if (frametime >= 0.1f) {
            if (up) {
                frame++;
                if (frame >= 5) {
                    frame = 3;
                    up = false;
                }
            } else {
                frame--;
                if (frame < 0) {
                    frame = 1;
                    up = true;
                }
            }
            frametime = 0;
        }

    }

    @Override
    protected void render() {
        main.draw_plant(null);

        for (Vector2 p : main.dead_leafs) {
            Main.batch.setColor(Main.greens[0]);
            Main.batch.draw(Res.deadleaf[frame], p.x, p.y);
        }

        Main.batch.setColor(Color.WHITE);

        Text.add("energy: " + showpoints, 102, Main.world_height - 10, true);

    }

    @Override
    public void dispose() {

    }
}

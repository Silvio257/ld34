package me.silviogames.ld34;


import com.badlogic.gdx.math.GridPoint2;

/**
 * Created by Silvio on 12.12.2015.
 */
public class PlantTile {

    public boolean xylem = false;

    public PlantTile(PlantTile before, int x, int y) {
        this.x = x;
        this.y = y;
        this.before = before;
        if (this.before != null) {
            this.before.after = this;
            set_type();
        } else {
            type = Plant.END_UP;
        }
    }

    PlantTile before, after;
    Plant type;
    private int x, y;
    public int water;

    public BuildingTile left = null, right = null;

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    public void add_building(BuildingTile bt){
        if(left == null){
            left = bt;
        }else{
            if(right == null){
                right = bt;
            }else{
                System.out.println("planttile is full");
            }
        }
    }

    public void remove_building(BuildingTile lr){
        if(lr == left){
            left = null;
        }else if(lr == right){
            right = null;
        }else{
            System.out.println("this buildingtile does not belong to me");
        }
    }

    public void set_type() {
        //dont call on first planttile
        int dx = before.x - x;
        int dy = before.y - y;

        if (dx == -1) {
            //right
            type = Plant.END_RIGHT;

        } else if (dx == 1) {
            //left
            type = Plant.END_LEFT;
        } else {
            if (dy == -1) {
                //up
                type = Plant.END_UP;
            } else if (dy == 1) {
                //down
                type = Plant.END_DOWN;
            }
        }
        if (type == null) {
            System.out.println("something went wrong");
        } else {
            before.bend(type);
        }

    }

    public PlantTile last() {
        if (this.after == null) {
            return this;
        } else {
            return after.last();
        }
    }

    public void bend(Plant dir) {
        switch (dir) {
            case END_UP:
                if (type == Plant.END_LEFT) {
                    type = Plant.CORNER_LOW_LEFT;
                } else if (type == Plant.END_UP) {
                    type = Plant.UP;
                } else if (type == Plant.END_RIGHT) {
                    type = Plant.CORNER_LOW_RIGHT;
                }
                break;
            case END_DOWN:
                if (type == Plant.END_LEFT) {
                    type = Plant.CORNER_UP_LEFT;
                } else if (type == Plant.END_RIGHT) {
                    type = Plant.CORNER_UP_RIGHT;
                } else if (type == Plant.END_DOWN) {
                    type = Plant.DOWN;
                }
                break;
            case END_LEFT:
                if (type == Plant.END_LEFT) {
                    type = Plant.LEFT;
                } else if (type == Plant.END_UP) {
                    type = Plant.CORNER_UP_RIGHT;
                } else if (type == Plant.END_DOWN) {
                    type = Plant.CORNER_LOW_RIGHT;
                }
                break;
            case END_RIGHT:
                if (type == Plant.END_RIGHT) {
                    type = Plant.RIGHT;
                } else if (type == Plant.END_UP) {
                    type = Plant.CORNER_UP_LEFT;
                } else if (type == Plant.END_DOWN) {
                    type = Plant.CORNER_LOW_LEFT;
                }
                break;
        }
    }

    public Plant visual_bend(GrowS.Grow_dir grow_dir) {
        Plant r = null;
        switch (grow_dir) {
            case UP:
                if (type == Plant.END_LEFT) {
                    r = Plant.CORNER_LOW_LEFT;
                } else if (type == Plant.END_UP) {
                    r = Plant.UP;
                } else if (type == Plant.END_RIGHT) {
                    r = Plant.CORNER_LOW_RIGHT;
                }
                break;
            case DOWN:
                if (type == Plant.END_LEFT) {
                    r = Plant.CORNER_UP_LEFT;
                } else if (type == Plant.END_RIGHT) {
                    r = Plant.CORNER_UP_RIGHT;
                } else if (type == Plant.END_DOWN) {
                    r = Plant.DOWN;
                }
                break;
            case LEFT:
                if (type == Plant.END_LEFT) {
                    r = Plant.LEFT;
                } else if (type == Plant.END_UP) {
                    r = Plant.CORNER_UP_RIGHT;
                } else if (type == Plant.END_DOWN) {
                    r = Plant.CORNER_LOW_RIGHT;
                }
                break;
            case RIGHT:
                if (type == Plant.END_RIGHT) {
                    r = Plant.RIGHT;
                } else if (type == Plant.END_UP) {
                    r = Plant.CORNER_UP_LEFT;
                } else if (type == Plant.END_DOWN) {
                    r = Plant.CORNER_LOW_LEFT;
                }
                break;
        }
        return r;
    }

    public boolean buildable(Main main) {
        boolean r = false;
        switch (type) {
            case END_UP:
            case END_DOWN:
            case END_RIGHT:
            case END_LEFT:
            case CORNER_LOW_LEFT:
            case CORNER_LOW_RIGHT:
            case CORNER_UP_LEFT:
            case CORNER_UP_RIGHT:
                r = false;
                break;
            case UP:
            case DOWN:
                r = main.free(x - 1, y) || main.free(x + 1, y);
                break;
            case LEFT:
            case RIGHT:
                r = main.free(x, y + 1) || main.free(x, y - 1);
                break;
        }

        return r;
    }

    public boolean twofree(Main main) {
        boolean r = false;
        switch (type) {
            case END_UP:
            case END_DOWN:
            case END_RIGHT:
            case END_LEFT:
            case CORNER_LOW_LEFT:
            case CORNER_LOW_RIGHT:
            case CORNER_UP_LEFT:
            case CORNER_UP_RIGHT:
                r = false;
                break;
            case UP:
            case DOWN:
                r = main.free(x - 1, y) && main.free(x + 1, y);
                break;
            case LEFT:
            case RIGHT:
                r = main.free(x, y + 1) && main.free(x, y - 1);
                break;
        }
        return r;
    }

    public GridPoint2 get_free(Main main) {
        GridPoint2 free = null;

        if (type == Plant.UP || type == Plant.DOWN) {
            if (main.free(x - 1, y)) {
                free = new GridPoint2(x - 1, y);
            } else if (main.free(x + 1, y)) {
                free = new GridPoint2(x + 1, y);
            }
        }
        if (type == Plant.LEFT || type == Plant.RIGHT) {
            if (main.free(x, y - 1)) {
                free = new GridPoint2(x, y - 1);
            } else if (main.free(x, y + 1)) {
                free = new GridPoint2(x, y + 1);
            }
        }

        return free;
    }


    public PlantTile next_xylem() {
        if (after == null) {
            return null;
        } else {
            if (!after.xylem && after.water >= 5) {
                return after;
            } else {
                return after.next_xylem();
            }
        }
    }

    public float energy(){
        int r = 0;
        if(left != null && left instanceof Leaf){
            r++;
        }if(right != null && right instanceof Leaf){
            r++;
        }
        return r / 2f;
    }


    public PlantTile next_buildable(Main main) {
        if (after == null) {
            return null;
        } else {
            if (after.buildable(main) && after.water >= 5) {
                return after;
            } else {
                return after.next_buildable(main);
            }
        }
    }

}

package me.silviogames.ld34;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.GridPoint2;

import me.silviogames.ld34.GrowS.*;

/**
 * Created by Silvio on 13.12.2015.
 */
public abstract class BuildingTile {

    public final int x, y;
    protected final PlantTile parent;
    protected final Grow_dir dir;


    public BuildingTile(int x, int y, PlantTile parent) {
        System.out.println("new building tile at x: " + x + " y: " + y);
        this.x = x;
        this.y = y;
        this.parent = parent;

        //// TODO: 13.12.2015 set growdir
        dir = GrowS.next_bend(parent, new GridPoint2(x, y));
    }

    public abstract void tick(Main main);

    public abstract void render(SpriteBatch batch);

}

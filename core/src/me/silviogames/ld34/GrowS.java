package me.silviogames.ld34;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.MathUtils;

import java.util.ArrayList;

/**
 * Created by Silvio on 13.12.2015.
 */
public class GrowS extends State {

    private float time = 6;

    private GridPoint2 cursor;
    private int index = 0;
    private ArrayList<GridPoint2> list;
    private int growcost;

    public GrowS(Main main) {
        super(main);

        list = main.get_possible_list();
        if (list.isEmpty()) {
            System.out.println("list is empty");
        } else {
            if (list.size() == 3) {
                index = 1;
            }
        }
        cursor = list.get(index);

        growcost = main.grow_cost();
    }

    @Override
    protected void update(float delta) {

        time -= delta * Main.backspeed;

        if (time <= 0) {
            Res.Sounds.ERROR.play();
            main.state = new GameS(main);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
            time = 6f;
            if (MathUtils.floor(main.energy) >= growcost) {
                main.energy -= growcost;
                PlantTile next = new PlantTile(main.start_plant.last(), cursor.x, cursor.y);
                if (cursor.x == 33) {
                    //win
                    Res.Sounds.WIN.play();
                    main.state = new WinState(main);
                    return;
                } else {
                    Res.Sounds.GROW.play();
                    main.map[cursor.x][cursor.y] = 1;
                    main.calculate_water();
                    growcost = main.grow_cost();
                }
            }
            if (MathUtils.floor(main.energy) < growcost || main.start_plant.last().water < 1) {
                main.state = new GameS(main);
            } else {
                index = 0;
                list = main.get_possible_list();
                if (list.isEmpty()) {
                    System.out.println("no more moves possible");
                } else {
                    if (list.size() == 3) {
                        index = 1;
                    }
                }
                cursor = list.get(index);
            }
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            //bend last plant
            time = 6f;
            if (list.size() > 1) {
                Res.Sounds.SELECT.play();
                index++;
                if (index >= list.size()) {
                    index = 0;
                }
                cursor = list.get(index);
            }
        }

    }

    @Override
    protected void render() {

        PlantTile now_plant = main.start_plant;
        while (now_plant != null) {
            if (now_plant == main.start_plant.last()) {
                //bend last tile to cursor
                Main.batch.setColor(Color.YELLOW);
                Grow_dir gd = next_bend(now_plant, cursor);
                if (gd == null) {
                    System.out.println("gd is null");
                }
                Plant bend = now_plant.visual_bend(gd);
                Main.batch.draw(Res.plant[bend.ordinal()], now_plant.x() * 6, now_plant.y() * 6);
            } else {
                if (now_plant.water >= 5) {
                    Main.batch.setColor(Main.greens[5]);
                } else {
                    Main.batch.setColor(Main.greens[now_plant.water]);
                }
                Main.batch.draw(Res.plant[now_plant.type.ordinal()], now_plant.x() * 6, now_plant.y() * 6);
                if (now_plant.left != null) {
                    now_plant.left.render(Main.batch);
                }
                if (now_plant.right != null) {
                    now_plant.right.render(Main.batch);
                }
            }
            now_plant = now_plant.after;
        }
        Main.batch.setColor(Color.YELLOW);
        Main.batch.draw(Res.plant[which_end(main.start_plant.last(), cursor).ordinal()], cursor.x * 6, cursor.y * 6);
        Main.batch.setColor(Color.WHITE);

        for (int x = 0; x < 34; x++) {
            for (int y = 0; y < 20; y++) {
                if (main.map[x][y] >= 12 && main.map[x][y] < 24) {
                    Main.batch.draw(Res.plant[main.map[x][y]], x * 6, y * 6);
                }
            }
        }

        Text.add("energy: " + MathUtils.floor(main.energy), 102, Main.world_height - 10, true);
        Text.add("growth cost: " + growcost, 102, Main.world_height - 20, true);

        Text.add("(D) direction", 170, Main.world_height - 20, true);
        Text.add("(A) grow", 30, Main.world_height - 20, true);
        main.back_text(time);
    }

    public enum Grow_dir {
        LEFT, RIGHT, UP, DOWN
    }

    public static Grow_dir next_bend(PlantTile last, GridPoint2 next) {
        int dx = last.x() - next.x;
        int dy = last.y() - next.y;

        if (dx == -1) {
            //right
            return Grow_dir.RIGHT;

        } else if (dx == 1) {
            //left
            return Grow_dir.LEFT;
        } else {
            if (dy == -1) {
                //up
                return Grow_dir.UP;
            } else if (dy == 1) {
                //down
                return Grow_dir.DOWN;
            } else {
                return null;
            }
        }
    }

    private Plant which_end(PlantTile last, GridPoint2 next) {
        int dx = last.x() - next.x;
        int dy = last.y() - next.y;

        if (dx == -1) {
            //right
            return Plant.END_RIGHT;
        } else if (dx == 1) {
            //left
            return Plant.END_LEFT;
        } else {
            if (dy == -1) {
                //up
                return Plant.END_UP;
            } else if (dy == 1) {
                //down
                return Plant.END_DOWN;
            } else {
                return null;
            }
        }
    }


    @Override
    public void dispose() {

    }
}

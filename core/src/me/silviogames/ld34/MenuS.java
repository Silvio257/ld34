package me.silviogames.ld34;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;

public class MenuS extends State {

    private float wait = 0;
    private String waitdots = "";
    private int volume = 4;
    private boolean adown, ddown;

    public MenuS(Main main) {
        super(main);
    }

    @Override
    protected void update(float delta) {

        if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
            adown = true;
        }
        if (adown && !Gdx.input.isKeyPressed(Input.Keys.A)) {
            volume--;
            if (volume <= 0) {
                volume = 0;
            }
            Res.volume = volume / 10.0f;
            Res.Sounds.VOLUME.play();
            adown = false;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            ddown = true;
        }
        if (ddown && !Gdx.input.isKeyPressed(Input.Keys.D)) {
            volume++;
            if (volume >= 9) {
                volume = 9;
            }
            Res.volume = volume / 10.0f;
            Res.Sounds.VOLUME.play();
            ddown = false;
        }
            if (Gdx.input.isKeyPressed(Input.Keys.A) && Gdx.input.isKeyPressed(Input.Keys.D)) {
                wait += delta;

                if (wait >= 0.8f) {
                    Res.Sounds.NEXT.play();
                    main.state = new SelectLevel(main);
                }
            } else {
                wait = 0;
            }

    }

    @Override
    protected void render() {

        Text.add("hold (A) and (D) to start", Main.world_width / 2, Main.world_height - 15, true);
        Text.add("press (A) or (D) to adjust volume", Main.world_width / 2, Main.world_height - 50, true);
        if (wait > 0) {
            int width = (int) (100 * (wait / 0.8f));
            Main.batch.draw(Res.pixel, (Main.world_width / 2) - (width / 2), Main.world_height - 20, width, 3);
        }

        for (int i = 0; i < 10; i++) {
            Main.batch.setColor(Color.DARK_GRAY);
            Main.batch.draw(Res.pixel, (Main.world_width / 2) - 20 + (i * 4), Main.world_height - 60, 3, 3);
        }

        for (int i = 0; i < volume + 1; i++) {
            Main.batch.setColor(Color.WHITE);
            Main.batch.draw(Res.pixel, (Main.world_width / 2) - 20 + (i * 4), Main.world_height - 60, 3, 3);
        }

        Text.add("silviogames(c) 2015", 137, 1);


    }

    @Override
    public void dispose() {

    }

}

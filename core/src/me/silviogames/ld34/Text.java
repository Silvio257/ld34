package me.silviogames.ld34;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pool.Poolable;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Silvio
 */
public class Text implements Poolable {

    private float x;
    private float y;
    private String message;
    private Color color;

    public static String alphabetString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789:.-_(),+" + "!?abcdefghijklmnopqrstuvwxyz/ ";
    public static Texture alphabetRAW;
    public static TextureRegion[] alphabet;
    private static List<Text> Texts = new ArrayList<Text>();
    private final static Pool<Text> textPool = new Pool<Text>() {
        @Override
        protected Text newObject() {
            return new Text();
        }

    };
    private float scale;

    /**
     *
     * @param x
     *            coordinate where the text is going to be drawn |int
     * @param y
     *            coordinate where the text is going to be drawn |int
     * @param message
     *            which will be draw |string
     * @param color
     *            which will be used to draw the text |
     * @param centered
     *            if the text will be drawn centered
     */
    public Text() {
        this.x = 0;
        this.y = 0;
        this.message = "";
        this.color = Color.WHITE;
        this.scale = 1;

    }

    public void initText(float x, float y, String message, Color color, boolean centered, float scale) {
        this.x = x;
        this.y = y;
        this.message = message;
        this.color = color;

        if (scale <= 0) {
            this.scale = 1;
        } else {
            this.scale = scale;
        }

        if (centered) {
            this.x = (int) (this.x - ((length(message, scale)) / 2));
        }
    }

    /**
     * run this methods once when creating the app to initialize the alphabet.
     */
    public static void init() {
        // <editor-fold defaultstate="collapsed" desc="load alphabet">
        // load alphabet
        alphabetRAW = new Texture(Gdx.files.internal("chars.png"));
        alphabet = new TextureRegion[74];
        alphabet[0] = new TextureRegion(alphabetRAW, 1, 1, 3, 5);// a
        alphabet[1] = new TextureRegion(alphabetRAW, 5, 1, 3, 5);// b
        alphabet[2] = new TextureRegion(alphabetRAW, 9, 1, 3, 5);// c
        alphabet[3] = new TextureRegion(alphabetRAW, 13, 1, 3, 5);// d
        alphabet[4] = new TextureRegion(alphabetRAW, 17, 1, 3, 5);// e
        alphabet[5] = new TextureRegion(alphabetRAW, 21, 1, 3, 5);// f
        alphabet[6] = new TextureRegion(alphabetRAW, 25, 1, 3, 5);// g
        alphabet[7] = new TextureRegion(alphabetRAW, 29, 1, 3, 5);// h
        alphabet[8] = new TextureRegion(alphabetRAW, 33, 1, 3, 5);// i
        alphabet[9] = new TextureRegion(alphabetRAW, 37, 1, 3, 5);// j
        alphabet[10] = new TextureRegion(alphabetRAW, 41, 1, 3, 5);// k
        alphabet[11] = new TextureRegion(alphabetRAW, 1, 7, 3, 5);// l
        alphabet[12] = new TextureRegion(alphabetRAW, 5, 7, 3, 5);// m
        alphabet[13] = new TextureRegion(alphabetRAW, 9, 7, 3, 5);// n
        alphabet[14] = new TextureRegion(alphabetRAW, 13, 7, 3, 5);// o
        alphabet[15] = new TextureRegion(alphabetRAW, 17, 7, 3, 5);// p
        alphabet[16] = new TextureRegion(alphabetRAW, 21, 7, 3, 5);// q
        alphabet[17] = new TextureRegion(alphabetRAW, 25, 7, 3, 5);// r
        alphabet[18] = new TextureRegion(alphabetRAW, 29, 7, 3, 5);// s
        alphabet[19] = new TextureRegion(alphabetRAW, 33, 7, 3, 5);// t
        alphabet[20] = new TextureRegion(alphabetRAW, 37, 7, 3, 5);// u
        alphabet[21] = new TextureRegion(alphabetRAW, 41, 7, 3, 5);// v
        alphabet[22] = new TextureRegion(alphabetRAW, 1, 13, 3, 5);// w
        alphabet[23] = new TextureRegion(alphabetRAW, 5, 13, 3, 5);// x
        alphabet[24] = new TextureRegion(alphabetRAW, 9, 13, 3, 5);// y
        alphabet[25] = new TextureRegion(alphabetRAW, 13, 13, 3, 5);// z
        alphabet[26] = new TextureRegion(alphabetRAW, 17, 13, 3, 5);// 0
        alphabet[27] = new TextureRegion(alphabetRAW, 21, 13, 3, 5);// 1
        alphabet[28] = new TextureRegion(alphabetRAW, 25, 13, 3, 5);// 2
        alphabet[29] = new TextureRegion(alphabetRAW, 29, 13, 3, 5);// 3
        alphabet[30] = new TextureRegion(alphabetRAW, 33, 13, 3, 5);// 4
        alphabet[31] = new TextureRegion(alphabetRAW, 37, 13, 3, 5);// 5
        alphabet[32] = new TextureRegion(alphabetRAW, 41, 13, 3, 5);// 6
        alphabet[33] = new TextureRegion(alphabetRAW, 1, 19, 3, 5);// 7
        alphabet[34] = new TextureRegion(alphabetRAW, 5, 19, 3, 5);// 8
        alphabet[35] = new TextureRegion(alphabetRAW, 9, 19, 3, 5);// 9
        alphabet[36] = new TextureRegion(alphabetRAW, 13, 19, 3, 5);// :
        alphabet[37] = new TextureRegion(alphabetRAW, 17, 19, 3, 5);// .
        alphabet[38] = new TextureRegion(alphabetRAW, 21, 19, 3, 5);// -
        alphabet[39] = new TextureRegion(alphabetRAW, 25, 19, 3, 5);// _
        alphabet[40] = new TextureRegion(alphabetRAW, 29, 19, 3, 5);// (
        alphabet[41] = new TextureRegion(alphabetRAW, 33, 19, 3, 5);// )
        alphabet[42] = new TextureRegion(alphabetRAW, 37, 19, 3, 5);// ,
        alphabet[43] = new TextureRegion(alphabetRAW, 41, 19, 3, 5);// +
        alphabet[44] = new TextureRegion(alphabetRAW, 1, 25, 3, 5);// !
        alphabet[45] = new TextureRegion(alphabetRAW, 5, 25, 3, 5);// ?
        alphabet[46] = new TextureRegion(alphabetRAW, 9, 25, 3, 5);// a
        alphabet[47] = new TextureRegion(alphabetRAW, 13, 25, 3, 5);// b
        alphabet[48] = new TextureRegion(alphabetRAW, 17, 25, 3, 5);// c
        alphabet[49] = new TextureRegion(alphabetRAW, 21, 25, 3, 5);// d
        alphabet[50] = new TextureRegion(alphabetRAW, 25, 25, 3, 5);// e
        alphabet[51] = new TextureRegion(alphabetRAW, 29, 25, 3, 5);// f
        alphabet[52] = new TextureRegion(alphabetRAW, 33, 25, 3, 5);// g
        alphabet[53] = new TextureRegion(alphabetRAW, 37, 25, 3, 5);// h
        alphabet[54] = new TextureRegion(alphabetRAW, 41, 25, 3, 5);// i
        alphabet[55] = new TextureRegion(alphabetRAW, 1, 31, 3, 5);// j
        alphabet[56] = new TextureRegion(alphabetRAW, 5, 31, 3, 5);// k
        alphabet[57] = new TextureRegion(alphabetRAW, 9, 31, 3, 5);// l
        alphabet[58] = new TextureRegion(alphabetRAW, 13, 31, 3, 5);// m
        alphabet[59] = new TextureRegion(alphabetRAW, 17, 31, 3, 5);// n
        alphabet[60] = new TextureRegion(alphabetRAW, 21, 31, 3, 5);// o
        alphabet[61] = new TextureRegion(alphabetRAW, 25, 31, 3, 5);// p
        alphabet[62] = new TextureRegion(alphabetRAW, 29, 31, 3, 5);// q
        alphabet[63] = new TextureRegion(alphabetRAW, 33, 31, 3, 5);// r
        alphabet[64] = new TextureRegion(alphabetRAW, 37, 31, 3, 5);// s
        alphabet[65] = new TextureRegion(alphabetRAW, 41, 31, 3, 5);// t
        alphabet[66] = new TextureRegion(alphabetRAW, 1, 37, 3, 5);// u
        alphabet[67] = new TextureRegion(alphabetRAW, 5, 37, 3, 5);// v
        alphabet[68] = new TextureRegion(alphabetRAW, 9, 37, 3, 5);// w
        alphabet[69] = new TextureRegion(alphabetRAW, 13, 37, 3, 5);// x
        alphabet[70] = new TextureRegion(alphabetRAW, 17, 37, 3, 5);// y
        alphabet[71] = new TextureRegion(alphabetRAW, 21, 37, 3, 5);// z
        alphabet[72] = new TextureRegion(alphabetRAW, 25, 37, 3, 5);///
        alphabet[73] = new TextureRegion(alphabetRAW, 29, 37, 3, 5);// leerzeichen
        // </editor-fold>
    }

    /**
     * convert the chars of the string to the index and draw the char from the
     * array alphabet
     *
     *
     * @param b
     *            Spritebatch
     */
    private void renderText(SpriteBatch b) {
        String buchstabe = "";
        if (message.length() > 0) {
            b.setColor(color);
            float off = 0;
            for (int i = 0; i < message.length(); i++) {
                buchstabe = message.substring(i, i + 1);
                if (buchstabe.equalsIgnoreCase("#")) {
                    b.setColor(Color.valueOf(message.substring(i + 1, i + 9)));
                    i += 8;
                } else {
                    b.draw(alphabet[charToIndex(buchstabe)], x + off, y, 0, 0, 3, 5, scale, scale, 0);
                    off += charOff(buchstabe) * scale;
                }
            }
            b.setColor(Color.WHITE);
        }
    }

    public static void render(SpriteBatch b) {
        for (Text t : Texts) {
            t.renderText(b);
            textPool.free(t);
        }
        Texts.clear();
    }

    /**
     * @param text
     *            that should be cleared of color codes
     * @return text without color codes in it.
     */
    public static String rawText(String text) {
        return text.replaceAll("#.{8}", "");
    }

    /*
     * converts string into index of array of char-images
     */
    private static int charToIndex(String buchstabe) {
        return alphabetString.indexOf(buchstabe) == -1 ? 72 : alphabetString.indexOf(buchstabe);
    }

    public static void addText(Text text) {
        Texts.add(text);
    }

    private static int charOff(String s) {
        int i = charToIndex(s);
        int returner = 0;
        switch (i) {
        case 36:
        case 37:
        case 44:
        case 54:
        case 57:
            returner = 2;
        break;
        case 40:
        case 41:
        case 42:
        case 55:
        case 63:
        case 64:
            returner = 3;
        break;
        default:
            returner = 4;
        break;
        }
        return returner;

    }

    public static String[] wrap(String text, int width) {
        if (rawText(text).length() <= width) {
            return new String[] { text };
        }
        int indexDone = 0;
        ArrayList<String> lineList = new ArrayList<String>();
        String[] wordList = rawText(text).split(" ");
        String[] rawWords = text.split(" ");

        String temp = "";
        String temp2 = "";
        while (indexDone < wordList.length) {
            if (temp.length() + 1 + wordList[indexDone].length() <= width) {
                // next word fits on the temp string
                temp = temp + wordList[indexDone] + " ";
                temp2 = temp2 + rawWords[indexDone] + " ";
                indexDone++;
                if (indexDone == wordList.length) {
                    lineList.add(temp2);
                }
            } else {
                // temp string is full
                lineList.add(temp2);
                temp = "";
                temp2 = "";
            }
        }
        for (String s : lineList) {
            s.trim();
        }

        return lineList.toArray(new String[] {});

    }

    public static float length(String text, float size) {
        float length = 0;
        if (text.length() > 0) {
            for (int i = 0; i < text.length(); i++) {
                String buchstabe = text.substring(i, i + 1);
                if (buchstabe.equalsIgnoreCase("#")) {
                    // color code found
                    i += 8;
                } else {
                    length += charOff(buchstabe) * size;
                }
            }
        }
        return length;
    }

    public static void dispose() {
        alphabetRAW.dispose();
    }

    public static void add(String text, float x, float y) {
        Text t = textPool.obtain();
        t.initText(x, y, text, Color.WHITE, false, 1);
        addText(t);
    }

    public static void add(String text, float x, float y, Color color) {
        Text t = textPool.obtain();
        t.initText(x, y, text, color, false, 1);
        addText(t);
    }

    public static void add(String text, float x, float y, boolean centered) {
        Text t = textPool.obtain();
        t.initText(x, y, text, Color.WHITE, centered, 1);
        addText(t);
    }

    public static void add(String text, float x, float y, float scale) {
        Text t = textPool.obtain();
        t.initText(x, y, text, Color.WHITE, false, scale);
        addText(t);
    }

    public static void add(String text, float x, float y, Color color, boolean centered) {
        Text t = textPool.obtain();
        t.initText(x, y, text, color, centered, 1);
        addText(t);
    }

    public static void add(String text, float x, float y, Color color, float scale) {
        Text t = textPool.obtain();
        t.initText(x, y, text, color, false, scale);
        addText(t);
    }

    public static void add(String text, float x, float y, boolean centered, float scale) {
        Text t = textPool.obtain();
        t.initText(x, y, text, Color.WHITE, centered, scale);
        addText(t);
    }

    public static void add(String text, float x, float y, Color color, boolean centered, float scale) {
        Text t = textPool.obtain();
        t.initText(x, y, text, color, centered, scale);
        addText(t);
    }

    @Override
    public void reset() {
        this.x = -10;
        this.y = -10;
        this.message = "";
        this.scale = 1;
        this.color = Color.WHITE;

    }

}

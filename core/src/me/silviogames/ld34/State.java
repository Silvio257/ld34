package me.silviogames.ld34;


public abstract class State {

    public Main main;

    public State(Main main) {
        this.main = main;
    }

    protected abstract void update(float delta);

    protected abstract void render();

    public abstract void dispose();


}

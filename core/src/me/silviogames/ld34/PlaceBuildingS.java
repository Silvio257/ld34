package me.silviogames.ld34;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.MathUtils;

import me.silviogames.ld34.Main.Building;

/**
 * Created by Silvio on 13.12.2015.
 */
public class PlaceBuildingS extends State {

    PlantTile cursor;

    Building b;
    private float time = 10;

    public PlaceBuildingS(Main main, Building b) {
        super(main);
        this.b = b;

        cursor = main.start_plant.next_buildable(main);
        if (cursor == null) {
            System.out.println("cursor is null");
            main.state = new GameS(main);
        }
        if (b == Building.Xylem) {
            System.out.println("b is xylem");
            main.state = new GameS(main);
        }
    }

    @Override
    protected void update(float delta) {
        time -= delta * Main.backspeed;

        if (time <= 0) {
            Res.Sounds.ERROR.play();
            main.state = new WhatBuildS(main);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
            //check if 2 sides possible
            if (cursor.twofree(main)) {
                Res.Sounds.SELECT.play();
                main.state = new Place2SidesBuildS(main, cursor, b);
                return;
            } else {
                //place it
                GridPoint2 place = cursor.get_free(main);
                if (place == null) {
                    System.out.println("place is null");
                } else {
                    //place building
                    main.energy -= b.cost;
                    main.map[place.x][place.y] = b.bytevalue;
                    switch (b) {
                        case Leaf:
                            cursor.add_building(new Leaf(place.x, place.y, cursor));
                            main.calculate_growth();
                            break;
                        case Flower:
                            cursor.add_building(new Flower(place.x, place.y, cursor));
                            break;
                        case Fruit:
                            cursor.add_building(new Fruit(place.x, place.y, cursor));
                            break;
                    }
                    Res.Sounds.GROW.play();
                    if (main.energy < b.cost) {
                        if (main.energy < 3) {
                            main.state = new GameS(main);
                        } else {
                            main.state = new WhatBuildS(main);
                        }
                    }
                    cursor = cursor.next_buildable(main);
                    if (cursor == null) {
                        //back to start
                        cursor = main.start_plant.next_buildable(main);
                        if (cursor == null) {
                            main.state = new GameS(main);
                        }
                    }
                }
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            time = 10;
            //find next plant
            Res.Sounds.SELECT.play();
            cursor = cursor.next_buildable(main);
            if (cursor == null) {
                //back to start
                cursor = main.start_plant.next_buildable(main);
                if (cursor == null) {
                    main.state = new GameS(main);
                }
            }
        }
    }

    @Override
    protected void render() {
        main.draw_plant(cursor);

        Text.add("energy: " + MathUtils.floor(main.energy), 102, Main.world_height - 10, true);
        Text.add("(D) move cursor", 136, Main.world_height - 20, true);
        Text.add("(A) select", 68, Main.world_height - 20, true);

        main.back_text(time);
    }

    @Override
    public void dispose() {

    }
}

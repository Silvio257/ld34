package me.silviogames.ld34;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Silvio on 13.12.2015.
 */
public class Flower extends BuildingTile {


    int age = 0;

    public Flower(int x, int y, PlantTile parent) {
        super(x, y, parent);
    }

    @Override
    public void tick(Main main) {
        age++;
        if (age >= 14) {
            parent.remove_building(this);
            main.map[x][y] = 0;
            Array<GridPoint2> enemylist = new Array<GridPoint2>();
            for (int x = 0; x < 34; x++) {
                for (int y = 0; y < 20; y++) {
                    if (main.map[x][y] >= 12 && main.map[x][y] < 24) {
                        enemylist.add(new GridPoint2(x, y));
                    }
                }
            }
            if (enemylist.size > 0) {
                GridPoint2 target = enemylist.get(MathUtils.random(enemylist.size - 1));
                main.map[target.x][target.y] = 0;
                Res.Sounds.REMOVE.play();
            }
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        int offx = 0;
        int offy = 0;
        switch (dir) {
            case LEFT:
                offx = 0;
                break;
            case RIGHT:
                offx = -2;
                break;
            case UP:
                offy = -2;
                break;
            case DOWN:
                offy = -1;
        }
        int showage = 0;
        showage = MathUtils.floor(age / 3f);
        batch.setColor(Color.WHITE);
        batch.draw(Res.flower[(dir.ordinal() * 5) + (showage)], (x * 6) + offx, (y * 6) + offy);

    }
}

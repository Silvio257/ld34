package me.silviogames.ld34;

import com.badlogic.gdx.math.MathUtils;

/**
 * Created by Silvio on 14.12.2015.
 */
public class WinState extends State {

    float time = 8f;

    public WinState(Main main) {
        super(main);
        main.levels_done.add(main.playing);
        main.playing = null;
        main.warning = false;
    }

    @Override
    protected void update(float delta) {
        time -= delta * Main.backspeed;

        if (time <= 0) {
            main.state = new SelectLevel(main);
        }


    }

    @Override
    protected void render() {
        Text.add("level completed!", 102, Main.world_height / 2, true, 1.5f);

        main.back_text(time);
    }

    @Override
    public void dispose() {

    }
}

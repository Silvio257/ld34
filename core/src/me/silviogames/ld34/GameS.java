package me.silviogames.ld34;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;


/**
 * Created by Silvio on 12.12.2015.
 */
public class GameS extends State {

    private Select select;

    public GameS(Main main) {
        super(main);
        select = Select.GROW;
    }

    @Override
    protected void update(float delta) {


        if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            //move cursor
            Res.Sounds.SELECT.play();
            switch (select) {
                case GROW:
                    select = Select.BUILD;
                    break;
                case BUILD:
                    select = Select.WAIT;
                    break;
                case WAIT:
                    select = Select.GROW;
                    break;
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
            switch (select) {
                case GROW:
                    if (main.energy >= main.grow_cost() && !main.get_possible_list().isEmpty() && main.start_plant.last().water >= 1) {
                        Res.Sounds.NEXT.play();
                        main.state = new GrowS(main);
                    } else {
                        Res.Sounds.ERROR.play();
                    }
                    break;
                case BUILD:
                    if (main.energy >= 3) {
                        if(main.start_plant.next_buildable(main) != null) {
                            Res.Sounds.NEXT.play();
                            main.state = new WhatBuildS(main);
                        }else{
                            Res.Sounds.NEXT.play();
                            main.state = new PlaceXylemS(main);
                        }
                    } else {
                        Res.Sounds.ERROR.play();
                    }
                    break;
                case WAIT:
                    Res.Sounds.NEXT.play();
                    main.state = new TickS(main);
                    break;
            }
        }

    }

    @Override
    protected void render() {

        main.draw_plant(null);

        // Text.add("last at: x_" + start_plant.last().x() + " y_" + start_plant.last().y(), 10, 130);


        Text.add("energy: " + MathUtils.floor(main.energy), 102, Main.world_height - 10, true);

        Text.add(select == Select.GROW ? "(Grow)" : "Grow", 51, Main.world_height - 20, true);
        Text.add(select == Select.BUILD ? "(Build)" : "Build", 102, Main.world_height - 20, true);
        Text.add(select == Select.WAIT ? "(Wait)" : "Wait", 153, Main.world_height - 20, true);

    }

    @Override
    public void dispose() {

    }


    private enum Select {
        GROW, BUILD, WAIT
    }

}

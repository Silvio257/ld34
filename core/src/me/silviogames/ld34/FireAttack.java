package me.silviogames.ld34;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Silvio on 14.12.2015.
 */
public class FireAttack extends State {

    Vector2 firepos, target_pos, dv;
    PlantTile target_tile;
    Fruit target_fruit;
    private boolean distracted = false;
    private int fire_frame = 0;
    private float frame_time = 0, fire_speed = 80, correct_time;


    public FireAttack(Main main) {
        super(main);

        Vector2 distract = null;
        target_pos = new Vector2(0, 0);
        PlantTile now = main.start_plant;
        firepos = new Vector2(210, 75);
        dv = new Vector2(0, 0);
        while (now != null) {
            if (now.left != null && now.left instanceof Fruit) {
                Fruit leftf = (Fruit) now.left;
                if (leftf.age >= 4) {
                    distract = new Vector2((now.left.x * 6) + 3, (now.left.y * 6) + 3);
                    target_fruit = (Fruit) now.left;
                    break;
                }
            } else if (now.right != null && now.right instanceof Fruit) {
                Fruit leftf = (Fruit) now.right;
                if (leftf.age >= 4) {
                    distract = new Vector2((now.right.x * 6) + 3, (now.right.y * 6) + 3);
                    target_fruit = (Fruit) now.right;
                    break;
                }
            }
            now = now.after;
        }
        if (distract == null) {
            // no fruit found
            distracted = false;
            //find random plant
            int plantlength = main.plant_length();
            int index = MathUtils.random((plantlength / 3) * 2, plantlength - 3);
            System.out.println("wanted index: " + index + " lenght: " + main.plant_length());
            target_tile = main.get(index);
            target_pos.x = ((target_tile.x() * 6) + 3);
            target_pos.y = ((target_tile.y() * 6) + 3);
        } else {
            distracted = true;
            target_pos = distract;
        }
        float angle = MathUtils.atan2(target_pos.y - firepos.y, target_pos.x - firepos.x);
        dv.x = MathUtils.cos(angle);
        dv.y = MathUtils.sin(angle);

    }

    @Override
    protected void update(float delta) {
        frame_time += delta;
        if (frame_time >= 0.1f) {
            frame_time = 0;
            fire_frame++;
            if (fire_frame >= 4) {
                fire_frame = 0;
            }
        }
        correct_time += delta;
        if (correct_time > 0.1f) {
            correct_time = 0;
            float angle = MathUtils.atan2(target_pos.y - firepos.y, target_pos.x - firepos.x);
            dv.x = MathUtils.cos(angle);
            dv.y = MathUtils.sin(angle);
        }

        fire_speed *= 1.003f;

        firepos.add(fire_speed * delta * dv.x, fire_speed * delta * dv.y);
        if (firepos.dst(target_pos) < 3) {
            if (distracted) {
                target_fruit.parent.remove_building(target_fruit);
                main.map[target_fruit.x][target_fruit.y] = 0;
                Array<Vector2> list = new Array<Vector2>();
                list.add(new Vector2((target_fruit.x * 6) + MathUtils.random(6), (target_fruit.y * 6) + MathUtils.random(6)));
                main.state = new BurnPlant(main, list, null);
                return ;
            } else {
                Array<Vector2> list = new Array<Vector2>();
                PlantTile now = target_tile;
                target_tile.before.left = null;
                target_tile.before.right = null;
                while (now != null) {
                    list.add(new Vector2((now.x() * 6) + MathUtils.random(6), (now.y() * 6) + MathUtils.random(6)));
                    if (now.left != null) {
                        main.map[now.left.x][now.left.y] = 0;
                        now.left = null;
                    }
                    if (now.right != null) {
                        main.map[now.right.x][now.right.y] = 0;
                        now.right = null;
                    }
                    main.map[now.x()][now.y()] = 0;
                    now = now.after;
                }
                main.state = new BurnPlant(main, list, target_tile);
                return;

            }
        }

    }

    @Override
    protected void render() {
        main.draw_plant(null);

        Main.batch.draw(Res.fire[fire_frame], firepos.x - 5, firepos.y - 5);

    }

    @Override
    public void dispose() {

    }
}

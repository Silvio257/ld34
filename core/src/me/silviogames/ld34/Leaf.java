package me.silviogames.ld34;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Silvio on 13.12.2015.
 */
public class Leaf extends BuildingTile {

    int age = 20;

    public Leaf(int x, int y, PlantTile parent) {
        super(x, y, parent);

    }

    @Override
    public void tick(Main main) {
        age--;
        if (age <= 0) {
            age = 0;
            parent.remove_building(this);
            main.map[x][y] = 0;
            main.dead_leafs.add(new Vector2((x * 6) + 3 , (y * 6) + 3));
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        if (age >= 5) {
            batch.setColor(Main.greens[5]);
        } else {
            batch.setColor(Main.greens[age]);
        }
        int offx = 0;
        int offy = 0;
        switch (dir) {
            case LEFT:
                offx = 0;
                break;
            case RIGHT:
                offx = -2;
                break;
            case UP:
                offy = -2;
                break;
            case DOWN:
                offy = -1;
        }
        batch.draw(Res.leaf[dir.ordinal()], (x * 6) + offx, (y * 6) + offy);
        batch.setColor(Color.WHITE);
    }
}

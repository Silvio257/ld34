package me.silviogames.ld34;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.MathUtils;

import me.silviogames.ld34.Main.Building;

/**
 * Created by Silvio on 13.12.2015.
 */
public class WhatBuildS extends State {

    private Building cursor;
    private float time = 6;

    public WhatBuildS(Main main) {
        super(main);
        cursor = Building.Xylem;
        if (main.energy < 3) {
            main.state = new GameS(main);
        }
    }

    @Override
    protected void update(float delta) {
        time -= delta * Main.backspeed;

        if (time <= 0) {
            Res.Sounds.ERROR.play();
            main.state = new GameS(main);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
            if (main.energy >= cursor.cost) {
                Res.Sounds.NEXT.play();
                if (cursor == Building.Xylem) {
                    main.state = new PlaceXylemS(main);
                } else {
                    main.state = new PlaceBuildingS(main, cursor);
                }
            } else {
                Res.Sounds.ERROR.play();
            }
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            Res.Sounds.SELECT.play();
            time = 6;
            switch (cursor) {
                case Xylem:
                    cursor = Building.Fruit;
                    break;
                case Fruit:
                    cursor = Building.Flower;
                    break;
                case Flower:
                    cursor = Building.Leaf;
                    break;
                case Leaf:
                    cursor = Building.Xylem;
                    break;
            }
        }
    }

    @Override
    protected void render() {
        Text.add("energy: " + MathUtils.floor(main.energy), 102, Main.world_height - 10, true);
        Text.add("(D) move cursor", 136, Main.world_height - 20, true);
        Text.add("(A) select", 68, Main.world_height - 20, true);

        Text.add(cursor == Main.Building.Xylem ? "(xylem)" : "xylem", 20, Main.world_height - 30, true);
        Text.add(cursor == Main.Building.Fruit ? "(fruit)" : "fruit", 20, Main.world_height - 50, true);
        Text.add(cursor == Building.Flower ? "(flower)" : "flower", 20, Main.world_height - 70, true);
        Text.add(cursor == Building.Leaf ? "(leaf)" : "leaf", 20, Main.world_height - 90, true);

        //building description
        switch (cursor) {
            case Xylem:
                Text.add("xylem moves water through the plant", 20, Main.world_height - 110);
                break;
            case Fruit:
                Text.add("fruits distract enemies from attacking", 20, Main.world_height - 110);
                Text.add("your plant", 20, Main.world_height - 120);
                break;
            case Flower:
                Text.add("flowers randomly remove", 20, Main.world_height - 110);
                Text.add("enemy plants", 20, Main.world_height - 120);
                break;
            case Leaf:
                Text.add("leafs provide energy by doing", 20, Main.world_height - 110);
                Text.add("CO2 + 2H2O + photons -) CH20 + O2", 20, Main.world_height - 120);
                break;
        }
        Text.add("energy cost: " + cursor.cost, 20, Main.world_height - 130);

        main.back_text(time);
    }

    @Override
    public void dispose() {

    }

}

package me.silviogames.ld34;

/**
 * Created by Silvio on 14.12.2015.
 */
public class InitGame extends State {

    public InitGame(Main main, Levels l) {
        super(main);
        main.playing = l;
        main.start_plant = new PlantTile(null, 1, 0);
        main.map[1][0] = 1;
        main.step = 0;
        main.energy = 15;
        main.growth = 1;
        main.warning = false;
        l.getmap(main.map, main);
        main.calculate_water();

    }

    @Override
    protected void update(float delta) {
        main.state = new GameS(main);
    }

    @Override
    protected void render() {

    }

    @Override
    public void dispose() {

    }
}

package me.silviogames.ld34;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.MathUtils;

import me.silviogames.ld34.Main.Building;

/**
 * Created by Silvio on 13.12.2015.
 */
public class Place2SidesBuildS extends State {

    PlantTile target;
    private float time = 10;

    GridPoint2 left, right;
    boolean one = false;
    Building b;

    public Place2SidesBuildS(Main main, PlantTile target, Building b) {
        super(main);
        this.target = target;
        this.b = b;
        if (target.type == Plant.UP || target.type == Plant.DOWN) {
            left = new GridPoint2(target.x() - 1, target.y());
            right = new GridPoint2(target.x() + 1, target.y());
        } else if (target.type == Plant.LEFT || target.type == Plant.RIGHT) {
            left = new GridPoint2(target.x(), target.y() - 1);
            right = new GridPoint2(target.x(), target.y() + 1);
        }
    }

    @Override
    protected void update(float delta) {
        time -= delta * Main.backspeed;

        if (time <= 0) {
            Res.Sounds.ERROR.play();
            main.state = new WhatBuildS(main);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
            main.energy -= b.cost;
            GridPoint2 place = one ? left : right;
            main.map[place.x][place.y] = b.bytevalue;
            switch (b) {
                case Leaf:
                    target.add_building(new Leaf(place.x, place.y, target));
                    main.calculate_growth();
                    break;
                case Flower:
                    target.add_building(new Flower(place.x, place.y, target));
                    break;
                case Fruit:
                    target.add_building(new Fruit(place.x, place.y, target));
                    break;
            }
            Res.Sounds.GROW.play();
            if (main.energy < b.cost) {
                if (main.energy < 3) {
                    main.state = new GameS(main);
                } else {
                    main.state = new WhatBuildS(main);
                }
            } else {
                main.state = new PlaceBuildingS(main, b);
            }

        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            time = 10;
            Res.Sounds.SELECT.play();
            one = !one;
        }


    }

    @Override
    protected void render() {
        main.draw_plant(target);

        Text.add("energy: " + MathUtils.floor(main.energy), 102, Main.world_height - 10, true);
        Text.add("(D) move", 136, Main.world_height - 20, true);
        Text.add("(A) place", 68, Main.world_height - 20, true);

        //// TODO: 13.12.2015 draw building in target position
        GridPoint2 p = one ? left : right;
        GrowS.Grow_dir d = GrowS.next_bend(target, p);
        int offx = 0;
        int offy = 0;
        switch (d) {
            case LEFT:
                offx = 0;
                break;
            case RIGHT:
                offx = -2;
                break;
            case UP:
                offy = -2;
                break;
            case DOWN:
                offy = -1;
        }
        Main.batch.setColor(Color.YELLOW);
        switch (b) {
            case Leaf:
                Main.batch.draw(Res.leaf[d.ordinal()], (p.x * 6) + offx, (p.y * 6) + offy);
                break;
            case Flower:
                Main.batch.draw(Res.flower[d.ordinal() * 5], (p.x * 6) + offx, (p.y * 6) + offy);
                break;
            case Fruit:
                Main.batch.draw(Res.fruit[d.ordinal() * 4], (p.x * 6) + offx, (p.y * 6) + offy);
                break;
        }

        main.back_text(time);
    }

    @Override
    public void dispose() {

    }
}

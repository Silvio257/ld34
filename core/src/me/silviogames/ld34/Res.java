package me.silviogames.ld34;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;

public class Res {

    private static Texture plant_raw, leaf_raw, deadleaf_raw, fire_raw;

    public static TextureRegion pixel;
    public static TextureRegion[] plant, leaf, fruit, flower, deadleaf, fire;
    protected static Map<Sounds, Sound> soundMap = new HashMap<Sounds, Sound>();

    public static float volume = 0.5f;

    public static void load() {

        pixel = new TextureRegion(Text.alphabetRAW, 2, 1, 1, 1);

        plant_raw = new Texture(Gdx.files.internal("plant.png"));
        plant = new TextureRegion[24];
        plant[0] = new TextureRegion(plant_raw, 0, 0, 6, 6); // up
        plant[1] = new TextureRegion(plant_raw, 0, 0, 6, 6); // down
        plant[2] = new TextureRegion(plant_raw, 7, 0, 6, 6); // left
        plant[3] = new TextureRegion(plant_raw, 7, 0, 6, 6); // right
        plant[4] = new TextureRegion(plant_raw, 14, 0, 6, 6); // corner upper left
        plant[5] = new TextureRegion(plant_raw, 21, 0, 6, 6); // corner upper right
        plant[6] = new TextureRegion(plant_raw, 28, 0, 6, 6); // corner lower right
        plant[7] = new TextureRegion(plant_raw, 35, 0, 6, 6); //corner lower left
        plant[8] = new TextureRegion(plant_raw, 42, 0, 6, 6); //end top
        plant[9] = new TextureRegion(plant_raw, 49, 0, 6, 6); // end bottom
        plant[10] = new TextureRegion(plant_raw, 56, 0, 6, 6); // end left
        plant[11] = new TextureRegion(plant_raw, 63, 0, 6, 6); // end right

        //enemy
        plant[12] = new TextureRegion(plant_raw, 70, 0, 6, 6); // up
        plant[13] = new TextureRegion(plant_raw, 70, 0, 6, 6); // down
        plant[14] = new TextureRegion(plant_raw, 77, 0, 6, 6); // left
        plant[15] = new TextureRegion(plant_raw, 77, 0, 6, 6); // right
        plant[16] = new TextureRegion(plant_raw, 84, 0, 6, 6); // corner upper left
        plant[17] = new TextureRegion(plant_raw, 91, 0, 6, 6); // corner upper right
        plant[18] = new TextureRegion(plant_raw, 98, 0, 6, 6); // corner lower right
        plant[19] = new TextureRegion(plant_raw, 105, 0, 6, 6); //corner lower left
        plant[20] = new TextureRegion(plant_raw, 112, 0, 6, 6); //end top
        plant[21] = new TextureRegion(plant_raw, 119, 0, 6, 6); // end bottom
        plant[22] = new TextureRegion(plant_raw, 126, 0, 6, 6); // end left
        plant[23] = new TextureRegion(plant_raw, 133, 0, 6, 6); // end right

        leaf_raw = new Texture(Gdx.files.internal("leaf.png"));

        leaf = new TextureRegion[4];
        leaf[0] = new TextureRegion(leaf_raw, 0, 0, 8, 8); //left
        leaf[1] = new TextureRegion(leaf_raw, 9, 0, 8, 8); //right
        leaf[2] = new TextureRegion(leaf_raw, 18, 0, 8, 8); //up
        leaf[3] = new TextureRegion(leaf_raw, 27, 0, 8, 8); //down

        fruit = new TextureRegion[16];
        fruit[0] = new TextureRegion(leaf_raw, 36, 0, 8, 8); //fruit small left
        fruit[1] = new TextureRegion(leaf_raw, 45, 0, 8, 8); //fruit small2 left
        fruit[2] = new TextureRegion(leaf_raw, 54, 0, 8, 8); //fruit middle left
        fruit[3] = new TextureRegion(leaf_raw, 63, 0, 8, 8); //fruit big left

        fruit[4] = new TextureRegion(leaf_raw, 72, 0, 8, 8); //fruit small right
        fruit[5] = new TextureRegion(leaf_raw, 81, 0, 8, 8); //fruit small2 right
        fruit[6] = new TextureRegion(leaf_raw, 90, 0, 8, 8); //fruit middle right
        fruit[7] = new TextureRegion(leaf_raw, 99, 0, 8, 8); //fruit big right

        fruit[8] = new TextureRegion(leaf_raw, 108, 0, 8, 8); //fruit small up
        fruit[9] = new TextureRegion(leaf_raw, 117, 0, 8, 8); //fruit small2 up
        fruit[10] = new TextureRegion(leaf_raw, 126, 0, 8, 8); //fruit middle up
        fruit[11] = new TextureRegion(leaf_raw, 135, 0, 8, 8); //fruit big up

        fruit[12] = new TextureRegion(leaf_raw, 144, 0, 8, 8); //fruit small down
        fruit[13] = new TextureRegion(leaf_raw, 153, 0, 8, 8); //fruit small2 down
        fruit[14] = new TextureRegion(leaf_raw, 162, 0, 8, 8); //fruit middle down
        fruit[15] = new TextureRegion(leaf_raw, 171, 0, 8, 8); //fruit big down

        flower = new TextureRegion[20];

        flower[0] = new TextureRegion(leaf_raw, 180, 0, 8, 8); // flower small left
        flower[1] = new TextureRegion(leaf_raw, 189, 0, 8, 8); // flower bigger left
        flower[2] = new TextureRegion(leaf_raw, 198, 0, 8, 8); // flower even bigger left
        flower[3] = new TextureRegion(leaf_raw, 207, 0, 8, 8); // flower much bigger left
        flower[4] = new TextureRegion(leaf_raw, 216, 0, 8, 8); // flower big left

        flower[5] = new TextureRegion(leaf_raw, 225, 0, 8, 8); // flower small right
        flower[6] = new TextureRegion(leaf_raw, 234, 0, 8, 8); // flower bigger right
        flower[7] = new TextureRegion(leaf_raw, 243, 0, 8, 8); // flower even bigger right
        flower[8] = new TextureRegion(leaf_raw, 252, 0, 8, 8); // flower much bigger right
        flower[9] = new TextureRegion(leaf_raw, 261, 0, 8, 8); // flower big right

        flower[10] = new TextureRegion(leaf_raw, 270, 0, 8, 8); // flower small up
        flower[11] = new TextureRegion(leaf_raw, 279, 0, 8, 8); // flower bigger up
        flower[12] = new TextureRegion(leaf_raw, 288, 0, 8, 8); // flower even bigger up
        flower[13] = new TextureRegion(leaf_raw, 297, 0, 8, 8); // flower much bigger up
        flower[14] = new TextureRegion(leaf_raw, 306, 0, 8, 8); // flower big up

        flower[15] = new TextureRegion(leaf_raw, 315, 0, 8, 8); // flower small right
        flower[16] = new TextureRegion(leaf_raw, 324, 0, 8, 8); // flower bigger right
        flower[17] = new TextureRegion(leaf_raw, 333, 0, 8, 8); // flower even bigger right
        flower[18] = new TextureRegion(leaf_raw, 342, 0, 8, 8); // flower much bigger right
        flower[19] = new TextureRegion(leaf_raw, 351, 0, 8, 8); // flower big right

        deadleaf_raw = new Texture("deadleaf.png");

        deadleaf = new TextureRegion[5];

        deadleaf[0] = new TextureRegion(deadleaf_raw, 0, 0, 10, 10);
        deadleaf[1] = new TextureRegion(deadleaf_raw, 11, 0, 10, 10);
        deadleaf[2] = new TextureRegion(deadleaf_raw, 22, 0, 10, 10);
        deadleaf[3] = new TextureRegion(deadleaf_raw, 33, 0, 10, 10);
        deadleaf[4] = new TextureRegion(deadleaf_raw, 44, 0, 10, 10);

        fire_raw = new Texture("fire.png");
        fire = new TextureRegion[4];
        fire[0] = new TextureRegion(fire_raw, 0, 0, 10, 10);
        fire[1] = new TextureRegion(fire_raw, 11, 0, 10, 10);
        fire[2] = new TextureRegion(fire_raw, 22, 0, 10, 10);
        fire[3] = new TextureRegion(fire_raw, 33, 0, 10, 10);


        System.out.println("Res: textures loaded");
    }

    public static void dispose() {
        System.out.println("Res: textures disposed");

        plant_raw.dispose();
        leaf_raw.dispose();
        deadleaf_raw.dispose();
        fire_raw.dispose();

        for (Entry<Sounds, Sound> e : soundMap.entrySet()) {
            e.getValue().dispose();
        }

        System.out.println("Res: sounds disposed");

    }

    public enum Sounds {

        NEXT, VOLUME, GROW, SELECT, ERROR, POINTS, WIN, FIRE,REMOVE;

        Sounds() {
            try {
                soundMap.put(this, Gdx.audio.newSound(Gdx.files.internal("sounds/" + this.toString().toLowerCase() + ".wav")));
            } catch (Exception e) {
                System.out.println("could not load " + this.toString());
            }
        }

        public void play() {
            soundMap.get(this).play(volume, MathUtils.random(0.8f, 1.2f), 0);
        }

    }

}

package me.silviogames.ld34;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.MathUtils;

/**
 * Created by Silvio on 14.12.2015.
 */
public class SelectLevel extends State {

    Levels cursor;

    public SelectLevel(Main main) {
        super(main);
        cursor = Levels.one;
    }

    @Override
    protected void update(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            //move cursor
            Res.Sounds.SELECT.play();
            switch (cursor) {
                case one:
                    cursor = Levels.two;
                    break;
                case two:
                    cursor = Levels.three;
                    break;
                case three:
                    cursor = Levels.four;
                    break;
                case four:
                    cursor = Levels.five;
                    break;
                case five:
                    cursor = Levels.one;
                    break;
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
            Res.Sounds.NEXT.play();
            main.state = new InitGame(main, cursor);
        }

    }

    @Override
    protected void render() {
        Text.add("select level", 102, Main.world_height - 10, true);
        Text.add("(D) move cursor", 136, Main.world_height - 20, true);
        Text.add("(A) select", 68, Main.world_height - 20, true);

        Text.add((cursor == Levels.one ? "(level one)" : "level one") + (main.levels_done.contains(Levels.one) ? " completed" : ""), 102, Main.world_height - 50, true);
        Text.add((cursor == Levels.two ? "(level two)" : "level two") + (main.levels_done.contains(Levels.two) ? " completed" : ""), 102, Main.world_height - 70, true);
        Text.add((cursor == Levels.three ? "(level three)" : "level three") + (main.levels_done.contains(Levels.three) ? " completed" : ""), 102, Main.world_height - 90, true);
        Text.add((cursor == Levels.four ? "(level four)" : "level four") + (main.levels_done.contains(Levels.four) ? " completed" : ""), 102, Main.world_height - 110, true);
        Text.add((cursor == Levels.five ? "(level five)" : "level five") + (main.levels_done.contains(Levels.five) ? " completed" : ""), 102, Main.world_height - 130, true);


    }

    @Override
    public void dispose() {

    }
}

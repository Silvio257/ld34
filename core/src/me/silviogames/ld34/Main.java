package me.silviogames.ld34;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;


public class Main implements ApplicationListener {
    public static SpriteBatch batch;
    OrthographicCamera camera;
    Viewport viewport;
    public static TextureRegion pixel;
    public State state;

    public static int world_width = 204, world_height = 150;
    public static String version = "0.1";

    public PlantTile start_plant;
    public byte[][] map;

    public Set<Levels> levels_done = new HashSet<Levels>();

    public Array<Vector2> dead_leafs = new Array<Vector2>();

    //0 free
    //1 plant stem
    //2 fruit
    //3 flower
    //4 leaf
    //5 enemy plant

    public float energy, growth = 1;
    public int step = 0;

    public static Color[] greens = new Color[6];
    public Levels playing;

    public static float backspeed = 2.5f;

    public boolean warning = false;
    private boolean up = false;
    private float uptime = 0;

    @Override
    public void create() {
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        viewport = new FitViewport(world_width, world_height, camera);
        Text.init();
        pixel = new TextureRegion(Text.alphabetRAW, 2, 1, 1, 1);

        Res.load();

        greens[5] = new Color(0.6f, 1f, 0.3f, 1);
        greens[4] = new Color(0.4f, 0.8f, 0.3f, 1);
        greens[3] = new Color(0.3f, 0.7f, 0.2f, 1);
        greens[2] = new Color(0.25f, 0.5f, 0.2f, 1);
        greens[1] = new Color(0.2f, 0.35f, 0.1f, 1);
        greens[0] = new Color(0.1f, 0.25f, 0f, 1);


        map = new byte[34][20];
        playing = null;

        state = new MenuS(this);


    }

    private void update(float delta) {

        state.update(delta);

        uptime += delta;
        if (uptime >= 0.5f) {
            up = !up;
            uptime = 0;
        }
    }

    @Override
    public void render() {
        update(Gdx.graphics.getDeltaTime());

        Gdx.gl.glClearColor(0.141f, 0.352f, 0.9f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        // tell the SpriteBatch to render in the
        // coordinate system specified by the camera.
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        state.render();

        if (warning) Text.add("!", 195, 75 + (up ? 1 : 0), Color.RED, 2);
        Text.render(batch);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }


    @Override
    public void dispose() {
        Text.dispose();
        state.dispose();
        Res.dispose();
    }

    public boolean free(int x, int y) {
        if (x >= 0 && x < 34 && y >= 0 && y < 20) {
            return map[x][y] == 0;
        } else {
            return false;
        }
    }

    public void grow(PlantTile last) {
        Plant type = last.type;
        ArrayList<GridPoint2> list = new ArrayList<GridPoint2>();
        //possible next positions are added to list
        switch (type) {
            case END_UP:
                if (free(last.x() - 1, last.y())) {
                    list.add(new GridPoint2(last.x() - 1, last.y()));
                }
                if (free(last.x(), last.y() + 1)) {
                    list.add(new GridPoint2(last.x(), last.y() + 1));
                }
                if (free(last.x() + 1, last.y())) {
                    list.add(new GridPoint2(last.x() + 1, last.y()));
                }
                break;
            case END_DOWN:
                if (free(last.x() + 1, last.y())) {
                    list.add(new GridPoint2(last.x() + 1, last.y()));
                }
                if (free(last.x(), last.y() - 1)) {
                    list.add(new GridPoint2(last.x(), last.y() - 1));
                }
                if (free(last.x() - 1, last.y())) {
                    list.add(new GridPoint2(last.x() - 1, last.y()));
                }
                break;
            case END_LEFT:
                if (free(last.x(), last.y() - 1)) {
                    list.add(new GridPoint2(last.x(), last.y() - 1));
                }
                if (free(last.x() - 1, last.y())) {
                    list.add(new GridPoint2(last.x() - 1, last.y()));
                }
                if (free(last.x(), last.y() + 1)) {
                    list.add(new GridPoint2(last.x(), last.y() + 1));
                }
                break;
            case END_RIGHT:
                if (free(last.x(), last.y() + 1)) {
                    list.add(new GridPoint2(last.x(), last.y() + 1));
                }
                if (free(last.x() + 1, last.y())) {
                    list.add(new GridPoint2(last.x() + 1, last.y()));
                }
                if (free(last.x(), last.y() - 1)) {
                    list.add(new GridPoint2(last.x(), last.y() - 1));
                }
                break;
        }
        if (list.isEmpty()) {
            System.out.println("could not grow");
        } else {
            GridPoint2 target = list.get(MathUtils.random(list.size() - 1));
            PlantTile next = new PlantTile(last, target.x, target.y);
            Res.Sounds.GROW.play();
            map[target.x][target.y] = 1;
        }
    }

    public ArrayList<GridPoint2> get_possible_list() {
        PlantTile last = start_plant.last();
        Plant type = last.type;
        ArrayList<GridPoint2> list = new ArrayList<GridPoint2>();

        switch (type) {
            case END_UP:
                if (free(last.x() - 1, last.y())) {
                    list.add(new GridPoint2(last.x() - 1, last.y()));
                }
                if (free(last.x(), last.y() + 1)) {
                    list.add(new GridPoint2(last.x(), last.y() + 1));
                }
                if (free(last.x() + 1, last.y())) {
                    list.add(new GridPoint2(last.x() + 1, last.y()));
                }
                break;
            case END_DOWN:
                if (free(last.x() + 1, last.y())) {
                    list.add(new GridPoint2(last.x() + 1, last.y()));
                }
                if (free(last.x(), last.y() - 1)) {
                    list.add(new GridPoint2(last.x(), last.y() - 1));
                }
                if (free(last.x() - 1, last.y())) {
                    list.add(new GridPoint2(last.x() - 1, last.y()));
                }
                break;
            case END_LEFT:
                if (free(last.x(), last.y() - 1)) {
                    list.add(new GridPoint2(last.x(), last.y() - 1));
                }
                if (free(last.x() - 1, last.y())) {
                    list.add(new GridPoint2(last.x() - 1, last.y()));
                }
                if (free(last.x(), last.y() + 1)) {
                    list.add(new GridPoint2(last.x(), last.y() + 1));
                }
                break;
            case END_RIGHT:
                if (free(last.x(), last.y() + 1)) {
                    list.add(new GridPoint2(last.x(), last.y() + 1));
                }
                if (free(last.x() + 1, last.y())) {
                    list.add(new GridPoint2(last.x() + 1, last.y()));
                }
                if (free(last.x(), last.y() - 1)) {
                    list.add(new GridPoint2(last.x(), last.y() - 1));
                }
                break;
        }

        return list;
    }

    public enum Building {
        Xylem(3, (byte) 0), Fruit(3, (byte) 2), Flower(5, (byte) 3), Leaf(4, (byte) 4);

        public int cost;
        public byte bytevalue;

        Building(int cost, byte bytevalue) {
            this.cost = cost;
            this.bytevalue = bytevalue;
        }
    }

    public void calculate_water() {
        start_plant.water = 10;
        PlantTile now = start_plant.after;
        while (now != null) {
            now.water = now.before.water - 1;
            if (now.water < 0) now.water = 0;
            if (now.water >= 5 && now.xylem) {
                now.water = 10;
            }
            now = now.after;
        }
    }

    public void calculate_growth() {
        PlantTile now = start_plant.after;
        growth = 1;
        while (now != null) {
            growth += now.energy();
            now = now.after;
        }
    }

    public void back_text(float time) {
        int seconds = MathUtils.floor(time);
        if (time < 5) {
            Text.add("back".substring(0, seconds), 10, world_height - 10);
        }
    }

    public PlantTile get(int index) {
        PlantTile r = null;
        int index2 = 0;
        PlantTile now = start_plant;
        while (now != null) {
            if (index2 == index) {
                r = now;
                break;
            } else {
                index2++;
                now = now.after;
            }
        }
        return r;
    }

    public int plant_length() {
        int r = 0;
        PlantTile now = start_plant;
        while (now != null) {
            r++;
            now = now.after;
        }
        return r;
    }

    public int grow_cost() {
        int length = 0;
        PlantTile now = start_plant;
        while (now != null) {
            length++;
            now = now.after;
        }
        return MathUtils.ceil(length / 7.0f);
    }

    public void draw_plant(PlantTile cursor) {
        if (cursor == null) {
            PlantTile now_plant = start_plant;
            while (now_plant != null) {
                if (now_plant.water >= 5) {
                    batch.setColor(greens[5]);
                } else {
                    batch.setColor(greens[now_plant.water]);
                }
                Main.batch.draw(Res.plant[now_plant.type.ordinal()], now_plant.x() * 6, now_plant.y() * 6);
                if (now_plant.left != null) {
                    now_plant.left.render(batch);
                }
                if (now_plant.right != null) {
                    now_plant.right.render(batch);
                }
                now_plant = now_plant.after;
            }
        } else {
            PlantTile now_plant = start_plant;
            while (now_plant != null) {
                if (now_plant == cursor) {
                    Main.batch.setColor(Color.CORAL);
                } else {
                    if (now_plant.water >= 5) {
                        batch.setColor(greens[5]);
                    } else {
                        batch.setColor(greens[now_plant.water]);
                    }
                }
                Main.batch.draw(Res.plant[now_plant.type.ordinal()], now_plant.x() * 6, now_plant.y() * 6);
                if (now_plant.left != null) {
                    now_plant.left.render(batch);
                }
                if (now_plant.right != null) {
                    now_plant.right.render(batch);
                }

                now_plant = now_plant.after;
            }
            Main.batch.setColor(Color.WHITE);
        }
        Main.batch.setColor(Color.WHITE);
        for (int x = 0; x < 34; x++) {
            for (int y = 0; y < 20; y++) {

                if (map[x][y] >= 12 && map[x][y] < 24) {
                    batch.draw(Res.plant[map[x][y]], x * 6, y * 6);
                }
            }
        }
    }
}

package me.silviogames.ld34;

/**
 * Created by Silvio on 12.12.2015.
 */
public enum Plant {
    UP, DOWN, LEFT, RIGHT, CORNER_UP_LEFT, CORNER_UP_RIGHT, CORNER_LOW_RIGHT, CORNER_LOW_LEFT, END_UP, END_DOWN, END_LEFT, END_RIGHT;
}

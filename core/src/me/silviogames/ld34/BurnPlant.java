package me.silviogames.ld34;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Silvio on 14.12.2015.
 */
public class BurnPlant extends State {

    Array<Vector2> list;

    float burntime = 0, frame_time;
    int fire_frame = 0;

    public BurnPlant(Main main, Array<Vector2> list, PlantTile destroy) {
        super(main);
        this.list = list;
        Res.Sounds.FIRE.play();
        if (destroy != null) {
            destroy.before.set_type();
            destroy.before.after = null;
        }
        main.calculate_growth();
        main.calculate_water();

    }

    @Override
    protected void update(float delta) {
        burntime += delta;
        if (burntime > 2) {
            main.state = new GameS(main);
        }
        frame_time += delta;
        if (frame_time >= 0.1f) {
            frame_time = 0;
            fire_frame++;
            if (fire_frame >= 4) {
                fire_frame = 0;
            }
        }
    }

    @Override
    protected void render() {

        main.draw_plant(null);
        for (Vector2 p : list) {
            Main.batch.draw(Res.fire[fire_frame], p.x - 5, p.y - 5);
        }
    }

    @Override
    public void dispose() {

    }
}

package me.silviogames.ld34;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Silvio on 13.12.2015.
 */
public class Fruit extends BuildingTile {

    int age = 0;

    public Fruit(int x, int y, PlantTile parent) {
        super(x, y, parent);
    }

    @Override
    public void tick(Main main) {
        age++;
    }

    @Override
    public void render(SpriteBatch batch) {
        int offx = 0;
        int offy = 0;
        switch (dir) {
            case LEFT:
                offx = 0;
                break;
            case RIGHT:
                offx = -2;
                break;
            case UP:
                offy = -2;
                break;
            case DOWN:
                offy = -1;
        }
        int showage = 0;
        if(age < 4){
            showage = age / 2;
        }else{
            showage = 3;
        }
        batch.setColor(Color.WHITE);
        batch.draw(Res.fruit[(dir.ordinal() * 4) + (showage)], (x * 6) + offx, (y * 6) + offy);
    }
}

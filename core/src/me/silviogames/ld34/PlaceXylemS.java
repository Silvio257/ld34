package me.silviogames.ld34;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;

/**
 * Created by Silvio on 13.12.2015.
 */
public class PlaceXylemS extends State {

    PlantTile cursor;
    private float time = 9;

    private boolean hold = false;

    public PlaceXylemS(Main main) {
        super(main);
        cursor = main.start_plant.next_xylem();
        if (cursor == null) {
            System.out.println("xylem null");
            main.state = new GameS(main);
        }
    }

    @Override
    protected void update(float delta) {
        time -= delta * Main.backspeed;

        if (time <= 0) {
            Res.Sounds.ERROR.play();
            if (main.energy < Main.Building.Xylem.cost) {
                main.state = new GameS(main);
            } else {
                main.state = new WhatBuildS(main);
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            time = 7;
            //find next plant
            Res.Sounds.SELECT.play();
            if (cursor.next_xylem() == null && !hold) {
                System.out.println("hold");
                hold = true;
            } else {
                hold = false;
                cursor = cursor.next_xylem();
                if (cursor == null) {
                    //back to start
                    cursor = main.start_plant.next_xylem();
                }
            }

        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
            cursor.xylem = true;
            main.energy -= Main.Building.Xylem.cost;
            Res.Sounds.GROW.play();
            main.calculate_water();
            if (main.energy < Main.Building.Xylem.cost) {
                main.state = new GameS(main);
            } else {
                cursor = main.start_plant.next_xylem();
                if (cursor == null) {
                    main.state = new WhatBuildS(main);
                }
            }
            time = 7;
        }
    }

    @Override
    protected void render() {
        main.draw_plant(cursor);

        Text.add("energy: " + MathUtils.floor(main.energy), 102, Main.world_height - 10, true);
        Text.add("(D) move cursor", 136, Main.world_height - 20, true);
        Text.add("(A) place xylem", 68, Main.world_height - 20, true);

        main.back_text(time);
    }

    @Override
    public void dispose() {

    }
}
